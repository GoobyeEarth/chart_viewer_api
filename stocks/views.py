import django_filters
from rest_framework import serializers
from rest_framework import viewsets
from django_filters import rest_framework as filters

from stocks.models import StockInfo
from stocks.models import Candle


class StockInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockInfo
        fields = ('id', 'code', 'name')


class StockInfoViewSet(viewsets.ModelViewSet):
    queryset = StockInfo.objects.all()
    serializer_class = StockInfoSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_fields = ('id', 'code')

class StockInfoFilterSet(filters.FilterSet):
    code = filters.CharFilter()

class CandleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Candle
        fields = ('date', 'high', 'start', 'end', 'low')

    def to_representation(self, instance):
        """
        Overwritten.
        :param instance:
        :return:
        """
        ret = super().to_representation(instance)
        return ret.values()


class CandleFilterSet(filters.FilterSet):
    stock_info = filters.NumberFilter(field_name="stock_info_id")
    date_gte = filters.DateFilter(field_name="date", lookup_expr='gte')


class CandleViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Candle.objects.all().order_by('date')
    serializer_class = CandleSerializer

    filterset_class = CandleFilterSet


