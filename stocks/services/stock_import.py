from urllib import request
from http.client import HTTPResponse
from typing import List
from typing import Dict
import time

from bs4 import BeautifulSoup
from bs4.element import Tag

from stocks.models import StockInfo
from stocks.models import Candle


class StockImportService(object):
    def __init__(self):
        pass

    def run(self, definitive: bool = False):
        stock_list = self.get_stock_list()
        if definitive:
            list_inserted = StockInfo.objects.bulk_create(stock_list)

        stock_list_inserted = StockInfo.objects.all()

        # あとで連続実行の考慮いるかも？でも3日程度でやり切れるなら必要ない。
        for stock_info in stock_list_inserted:
            time.sleep(1)
            print(stock_info.name)
            print(stock_info.id)

            url = self._retrieve_candle_url(stock_info.code)
            soup = self.get_soup(url)
            imds = self.parse_candles(soup)

            candles = [Candle.create_from_str(stock_info, imd) for imd in imds]
            if definitive:
                Candle.objects.bulk_create(candles)

    def get_stock_list(self) -> List[StockInfo]:
        page_num = 34
        stock_list = []

        for page_no in range(1, 2):
            time.sleep(1)
            print(page_no)
            url = self._retrieve_stock_list_url(page_no)
            soup = self.get_soup(url)
            stock_list.extend(self.parse_stock_info(soup))

        return stock_list

    def get_soup(self, url) -> BeautifulSoup:
        html: HTTPResponse = request.urlopen(url)

        html_str: str = html.read().decode('utf-8')
        html_str_replaced = html_str.replace('<tbody>', '')
        html_str_replaced = html_str_replaced.replace('</tbody>', '')

        soup = BeautifulSoup(html_str_replaced, 'html.parser')


        return soup

    @staticmethod
    def _retrieve_stock_list_url(page_no: int) -> str:
        base_url = 'https://kabuoji3.com/stock/'
        return base_url + '?page=' + str(page_no)

    @staticmethod
    def _retrieve_candle_url(stock_code: str) -> str:
        base_url = 'https://kabuoji3.com/stock/'
        return base_url + stock_code + '/'

    def parse_stock_info(self, soup: BeautifulSoup) -> List[StockInfo]:
        stock_info_list = []
        tables: List[Tag] = soup.select('.stock_table')

        for table in tables:
            trs: List[Tag] = table.select('tr')
            for tr in trs:
                tds: List[Tag] = tr.select('td')
                row_str = [td.text for td in tds]
                if len(row_str) > 0:
                    stock_info_list.append(self._get_one_stock_info(row_str))

        return stock_info_list

    @staticmethod
    def _get_one_stock_info(imd: List[str]) -> StockInfo:
        stock_info = StockInfo()
        stock_info.name = ' '.join(imd[0].split(' ')[1:])
        stock_info.code = imd[0].split(' ')[0]

        return stock_info


    def parse_candles(self, soup: BeautifulSoup) -> List[Dict[str, str]]:
        tables: List[Tag] = soup.select('.stock_data_table')

        candles = []
        for table in tables:
            trs: List[Tag] = table.select('tr')
            for tr in trs:
                tds: List[Tag] = tr.select('td')
                row_str = [td.text for td in tds]
                if len(row_str) > 0:
                    candle = self.parse_one_candle(row_str)
                    candles.append(candle)

        return candles

    @staticmethod
    def parse_one_candle(row_str: List[str]) -> Dict[str, str]:
        ret = {
            'date': row_str[0],
            'start': row_str[1],
            'high': row_str[2],
            'low': row_str[3],
            'end': row_str[4],
            'volume': row_str[5],
        }

        return ret
