from urllib import request
from http.client import HTTPResponse
from typing import List
from typing import Dict

from django.core.management.base import BaseCommand
from bs4 import BeautifulSoup
from bs4.element import Tag

from stocks.models import StockInfo
from stocks.models import Candle
from stocks.services.stock_import import StockImportService


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        service = StockImportService()
        service.run(definitive=True)



