from rest_framework import routers

from stocks.views import StockInfoViewSet
from stocks.views import CandleViewSet

router = routers.DefaultRouter()
router.register('info', StockInfoViewSet)
router.register('candle', CandleViewSet)
