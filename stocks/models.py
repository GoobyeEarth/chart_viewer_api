from datetime import datetime
from typing import Dict

from django.db import models


# Create your models here.

class StockInfo(models.Model):
    code = models.CharField(max_length=16, default='', db_index=True)
    name = models.CharField(max_length=32, default='', db_index=True)

    def __str__(self):
        return self.name


class Candle(models.Model):
    date = models.DateField(db_index=True)
    start = models.IntegerField(default=0)
    high = models.IntegerField(default=0)
    low = models.IntegerField(default=0)
    end = models.IntegerField(default=0)
    # 出来高、金額ではなく取引枚数
    volume = models.IntegerField(default=0)
    stock_info = models.ForeignKey(StockInfo,
                                   on_delete=models.CASCADE,
                                   default=0,
                                   db_index=True)

    @staticmethod
    def create_from_str(stock_info: StockInfo, candle_str: Dict[str, str]):
        candle = Candle()
        candle.stock_info = stock_info

        date = datetime.strptime(candle_str['date'], '%Y-%m-%d').date()
        candle.date = date
        candle.start = int(candle_str['start'])
        candle.high = int(candle_str['high'])
        candle.low = int(candle_str['low'])
        candle.end = int(candle_str['end'])
        candle.volume = int(candle_str['volume'])

        return candle

    def __str__(self):
        return str(self.date)


