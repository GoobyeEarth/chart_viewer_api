from django.contrib import admin

# Register your models here.
from stocks.models import StockInfo
from stocks.models import Candle


admin.site.register(StockInfo)
admin.site.register(Candle)
