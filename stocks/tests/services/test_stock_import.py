from django.test import TestCase
from bs4 import BeautifulSoup

from stocks.services.stock_import import StockImportService


class TestStockImportService(TestCase):
    def test__retrieve_stock_list_url(self):
        url = StockImportService._retrieve_stock_list_url(1)
        self.assertEqual(url, 'https://kabuoji3.com/stock/?page=1')


    def test_parse_stock_info(self):
        service = StockImportService()
        fp = open('stocks/tests/files/stock_info_sample.html', mode='r')
        soup = BeautifulSoup(fp, 'html.parser')
        result = service.parse_stock_info(soup)
        self.assertEqual(len(result), 120)

    def test__get_one_stock_info(self):
        result = StockImportService._get_one_stock_info(['1414 ショーボンドホールディングス(株)', '東証1部', '4180', '4185', '4150', '4180'])
        self.assertEqual(result.code, '1414')
        self.assertEqual(result.name, 'ショーボンドホールディングス(株)')

    def test_to_dict(self):
        row_str = ['2018-06-14', '13430', '13440', '13450', '13460', '32', '13440']
        result = StockImportService.parse_one_candle(row_str)
        self.assertEqual(result,
                         {'date': '2018-06-14', 'start': '13430', 'high': '13440', 'low': '13450',
                          'end': '13460', 'volume': '32'})
