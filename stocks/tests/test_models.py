from django.test import TestCase

from stocks.models import Candle
from stocks.models import StockInfo


class CandleTest(TestCase):
    def test_create_from_str(self):

        stock_info = StockInfo()
        stock_info.code = '1111'
        stock_info.code = 'hoge'
        candle = Candle.create_from_str(stock_info, {
            'code': '1498',
            'date': '2018-06-11',
            'start': '13490',
            'high': '13491',
            'low': '13492',
            'end': '13493',
            'volume': '8'
        })

        self.assertEqual(candle.date.day, 11)
        self.assertEqual(candle.start, 13490)
        self.assertEqual(candle.high, 13491)
        self.assertEqual(candle.low, 13492)
        self.assertEqual(candle.end, 13493)
        self.assertEqual(candle.volume, 8)


